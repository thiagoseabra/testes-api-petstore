
public enum Animal {
	
	Brutus(1, 1, " Dog", "Brutus", 1, "Pastor Alem�o", "available"), 
	Rex(2, 1, "Dog", "Rex", 2, "Boxer", "available"),
	Amora(3, 2, "Cat", "Amora", 4, "Siames", "available"), 
	Katie(4, 1, "Dog", "Katie", 3, "Pincher", "available"),
	Thor(5, 2, "Cat", "Thor", 10, "RagDoll", "available"), 
	kittie(6, 2, "Cat", "kittie", 5, "Angor�", "available"),
	Frank(7, 1, "Dog", "Frank", 11, "Buldogue", "available"),
	Alladin(8, 2, "Cat", "Alladin", 6, "Siberiano", "available"),
	Dexter(9, 2, "Cat", "Dexter", 7, "Devon", "available"), 
	Diana(10, 1, "Dog", "Diana", 9, "Poodle", "available");

	private final int id;
	private final int Categoriaid;
	private final String CategoriaNome;
	private final String Nome;
	private final int RacaId;
	private final String RacaNome;
	private final String petStatus;

	Animal(int id, int Categoriaid, String CategoriaNome, String Nome, int RacaId, String RacaNome, String Status) {
		this.id = id;
		this.Categoriaid = Categoriaid;
		this.CategoriaNome = CategoriaNome;
		this.Nome = Nome;
		this.RacaId = RacaId;
		this.RacaNome = RacaNome;
		this.petStatus = Status;
	}

	public int getId() {
		return id;
	}

	public int getCategoriaid() {
		return Categoriaid;
	}

	public String getCategoriaNome() {
		return CategoriaNome;
	}

	public String getNome() {
		return Nome;
	}

	public int getRacaId() {
		return RacaId;
	}

	public String getRacaNome() {
		return RacaNome;
	}

	public String getPetStatus() {
		return petStatus;
	}

	public static Animal getAnimal(int id) {

		for (Animal A : Animal.values()) {
			if (id == (A.getId())) {

				return A;
			}
		}
		throw new IllegalArgumentException();

	}

	
	
	public static Animal getnAnimal(String Categoria) {

		for (Animal d : Animal.values()) {
			if (Categoria.equalsIgnoreCase(d.getCategoriaNome())) {
				
				return d;
				
			}
		}
		throw new IllegalArgumentException();

	}
	
	
	
	public static String getjsonAnimal(int id) {

		for (Animal d : Animal.values()) {
			if (id == (d.getId())) {

				String pet = "{\"id\": " + d.getId() + ",\"category\": {\"id\":" + d.getCategoriaid() + ",\"name\": \""
						+ d.getCategoriaNome() + "\"},\"name\": \"" + d.getNome()
						+ "\",\"photoUrls\": [],\"tags\": [{\"id\":" + d.getRacaId() + ",\"name\": \"" + d.getRacaNome()
						+ "\"}],\"status\": \"" + d.getPetStatus() + "\"}";

				return pet;
			}
		}
		throw new IllegalArgumentException();

	}
	
	
	
	

}

import java.io.IOException;
import javax.swing.JOptionPane;
import com.google.gson.Gson;


public class Calls {

	public static void main(String[] args) throws IOException {
		
		//Criando o cliente
		Cliente Maria = new Cliente(1, "Maria Assuncao", "Maria", "Assuncao", "123456", "15-995888477", 1);
		Cliente Thiago = new Cliente(2, "Thiago Silva", "Thiago", "Silva", "951456", "15-58874140", 0);
		Cliente Fernando = new Cliente(3, "Fernando Fernandes", "Fernando", "Fernandes", "159357", "11-954412558", 1);
		Cliente Thais = new Cliente(4, "Thais Oliveira", "thais", "Oliveira", "987654", "11-58996553", 2);
		Cliente Jorge = new Cliente(6, "Jorge Araujo", "Jorge", "Araujo", "963258", "11-54845699", 0);
		
		//pegando o PET com id 1
		Animal pet = Animal.getAnimal(1);
		
		// Urls de Comunica��o com a API
		String urlCliente = "https://petstore.swagger.io/v2/user/";
		String urlAnimal =  "https://petstore.swagger.io/v2/pet/";
		String urlPedido =  "https://petstore.swagger.io/v2/store/order/";
		
		//Gerar o json
		Gson gson = new Gson();
		
		//Post para Cria��o do Cliente (Maria)		
		Requests.SendPost(gson.toJson(Maria), urlCliente);
		
		//Post para Cria��o do Pet (Brutus)
		Requests.SendPost(Animal.getjsonAnimal(pet.getId()), urlAnimal );
		
		//Get para Consulta do cadastro do cliente
		Requests.SendGet(urlCliente + Maria.getUsername());
		
		//Get para consulta do cadastro do pet
		Requests.SendGet(urlAnimal + pet.getId());
		
		//Logando como Cliente
		Requests.SendGet(urlCliente +"login?username="+Maria.getUsername()+"&password="+Maria.getPassword());
		
		//Realizando a Compra do pet
		Pedido Brutus = new Pedido(1, pet.getId(), 1,"2021-05-27" , "Aproved" , false);
		Requests.SendPost(gson.toJson(Brutus), urlPedido);
		
		//Atualizando a Ordem de Comnpra
		Brutus.setStatus("Delivered");
		Requests.SendPost(gson.toJson(Brutus), urlPedido);
		
		//Verificando se houve a atualiza��o
		String resposta = Requests.SendGet(urlPedido + Brutus.getId(), "status");
		
		if(resposta.equalsIgnoreCase("Delivered")) {
			JOptionPane.showMessageDialog(null, "Testes Realizados com sucesso!!");
		}
		else{
			JOptionPane.showMessageDialog(null, "Houve divergencia na resposta Esperada/nResposta Recebida:"+ resposta);
		}
		
	}

}


public class Pedido {
	
	private final int id;
	private final int petId;
	private final int quantity;
	private final String shipDate;
	private String status;
	private final Boolean complete;
	
	Pedido(int id, int petId, int quantity, String Data, String status, Boolean complete) {
		super();
		this.id = id;
		this.petId = petId;
		this.quantity = quantity;
		this.shipDate = Data;
		this.status = status;
		this.complete = complete;
	}
	
	
	public int getId() {
		return id;
	}
	public int getPetId() {
		return petId;
	}
	public int getQuantity() {
		return quantity;
	}
	public String getShipDate() {
		return shipDate;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	public Boolean getComplete() {
		return complete;
	}
	
	
	
	

}

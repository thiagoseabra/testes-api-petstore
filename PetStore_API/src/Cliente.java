
public class Cliente {
	
	private final int id;
	private final String username;
	private final String firstName;
	private final String lastName;
	private final String Email;
	private final String password;
	private final String phone;
	private final int userStatus;

	Cliente(int id, String username, String Nome, String Sobrenome, String senha, String Telefone,
			int Status) {
		this.id = id;
		this.username = username;
		this.firstName = Nome;
		this.lastName = Sobrenome;
		Email = username+"@gmail.com";
		this.password = senha;
		this.phone = Telefone;
		this.userStatus = Status;
	}

	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}
	
	public String getEmail() {
		return Email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPassword() {
		return password;
	}

	public String getPhone() {
		return phone;
	}

	public int getUserStatus() {
		return userStatus;
	}
	
	
}

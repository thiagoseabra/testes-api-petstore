import java.io.IOException;
import javax.swing.JOptionPane;
import org.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Requests {

	public static void SendPost(String mensagem, String url) throws IOException {

		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		@SuppressWarnings("deprecation")
		RequestBody body = RequestBody.create(mediaType, mensagem);
		Request request = new Request.Builder().url(url).method("POST", body)
				.addHeader("Content-Type", "application/json").build();
		try (Response response = client.newCall(request).execute()) {
			if (!response.isSuccessful()) {
				JOptionPane.showMessageDialog(null,"Erro " + response.code() + "ao obter dados da URL" );
				System.exit(0);
			}
			else {
				System.out.println("C�digo de Resposta:" + response.code());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void SendGet(String url) throws IOException {

		OkHttpClient client = new OkHttpClient().newBuilder().build();
		Request request = new Request.Builder().url(url).method("GET", null).addHeader("Content-Type", "application/json").build();
		Response response = client.newCall(request).execute();
		try (response) {
			if (!response.isSuccessful()) {
				JOptionPane.showMessageDialog(null,"Erro " + response.code() + " ao obter dados da URL");
				System.exit(0);
			}
			else {
				String jsonData = response.body().string();
			    JSONObject Jobject = new JSONObject(jsonData);
			    System.out.println(Jobject);				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static String SendGet(String url, String Validador) throws IOException {

		OkHttpClient client = new OkHttpClient().newBuilder().build();
		Request request = new Request.Builder().url(url).method("GET", null).addHeader("Content-Type", "application/json").build();
		Response response = client.newCall(request).execute();
		try (response) {
			if (!response.isSuccessful()) {
				JOptionPane.showMessageDialog(null,"Erro " + response.code() + " ao obter dados da URL");
				System.exit(0);
			}
			else {
				String jsonData = response.body().string();
			    JSONObject Jobject = new JSONObject(jsonData);
			    return Jobject.getString(Validador);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response.toString();
	}
	
}
